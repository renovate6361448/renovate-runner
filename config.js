module.exports = {
    labels: ["dependencies"],
    platform: 'gitlab',
    hostRules: [
      {
        hostType: 'npm',
        matchHost: 'https://gitlab.vsa-digital.vnet.valeo.com/api/v4/packages/npm/',
        token: process.env.RENOVATE_TOKEN, 
      }
    ],
    includeForks: true, 
    platformAutomerge: true,
    packageRules: [
      {
        "description": "Auto merge small changes as per https://docs.renovatebot.com/configuration-options/#automerge",
        "matchUpdateTypes": ["patch", "pin", "digest"], 
        "automerge": true
      }
    ]
    // "baseBranches": ["master"]
  };
  